## Lilishop 技术栈 学习

##### 官方公众号 & 开源不易，如有帮助请点Star
![image-20210511171611793](https://pickmall.cn/assets/imgs/h5-qrcode.png)

[![star](https://gitee.com/beijing_hongye_huicheng/lilishop/badge/star.svg?theme=dark)](https://gitee.com/beijing_hongye_huicheng/lilishop/stargazers)

![github](https://img.shields.io/github/stars/hongyehuicheng/lilishop.svg?style=social&logo=#181717)


### 介绍
**官网**：https://pickmall.cn

Lilishop 是一款Java开发，基于SpringBoot研发的B2B2C多用户商城，前端使用 Vue、uniapp开发 **系统全端全部代码开源**

本系统用于教大家如何运用系统中的每一个细节，如：支付、第三方登录、日志收集、分布式事务、秒杀场景等各个场景学习方案



### 目录

#### 分布式

[ELK日志收集](./elk)


[分布式延时任务](./time-trigger)

[k8s集群安装]

[分布式锁]

[分布式事务]

[限流]

[分布式定时任务]

#### 

#### 其他

[JWT]

[第三方支付集成]

[第三方登录集成]

[SSO]

[流量统计]

#### 中间件集成篇

[Swagger-ui]

[Rocketmq]

[Elasticsearch 商品索引]

[Mongodb]

[Rocketmq]

[redis]

#### 

#### 微服务

SpringCloudAlibaba

#### 中台